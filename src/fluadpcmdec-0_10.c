/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

static GstElementClass *parent_class = NULL;
static void gst_fluadpcmdec_class_init (GstFluAdpcmDecClass * klass);
static void gst_fluadpcmdec_init (GstFluAdpcmDec * dec,
    GstFluAdpcmDecClass * dec_class);

GType
gst_fluadpcmdec_get_type (void)
{
  static volatile gsize fluadpcmdec_type = 0;

  if (g_once_init_enter (&fluadpcmdec_type)) {
    GType _type;
    static const GTypeInfo fluadpcmdec_info = {
      sizeof (GstFluAdpcmDecClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_fluadpcmdec_class_init,
      NULL,
      NULL,
      sizeof (GstFluAdpcmDec),
      0,
      (GInstanceInitFunc) gst_fluadpcmdec_init,
    };

    _type = g_type_register_static (GST_TYPE_ELEMENT,
        "GstFluAdpcmDec", &fluadpcmdec_info, G_TYPE_FLAG_ABSTRACT);
    g_once_init_leave (&fluadpcmdec_type, _type);
  }
  return fluadpcmdec_type;
}

static gboolean
gst_fluadpcmdec_setcaps (GstPad * pad, GstCaps * caps)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (gst_pad_get_parent (pad));
  GstCaps *out_caps;
  gboolean ret = FALSE;

  GST_DEBUG_OBJECT (dec, "setcaps called with %" GST_PTR_FORMAT, caps);

  if (_set_format (GST_ELEMENT (dec), caps)) {

    dec->duration = gst_util_uint64_scale (dec->num_compressed_samples,
        GST_SECOND, dec->rate);

    out_caps = gst_caps_new_simple ("audio/x-raw-int",
        "channels", G_TYPE_INT, dec->channels,
        "width", G_TYPE_INT, 16,
        "depth", G_TYPE_INT, 16,
        "rate", G_TYPE_INT, dec->rate,
        "signed", G_TYPE_BOOLEAN, TRUE,
        "endianness", G_TYPE_INT, G_BYTE_ORDER, NULL);

    gst_pad_set_caps (dec->srcpad, out_caps);
    gst_caps_unref (out_caps);

    ret = TRUE;
  }

  gst_object_unref (dec);
  return ret;
}

static inline GstBuffer *
gst_fluadpcmdec_clip (GstFluAdpcmDec * dec, GstBuffer * buffer)
{
  gint64 clip_start = 0, clip_stop = 0, start = 0, stop = 0;
  gboolean in_seg = FALSE;
  gint64 buf_size = 0;

  buf_size = GST_BUFFER_SIZE (buffer);
  start = GST_BUFFER_TIMESTAMP (buffer);
  stop = start + GST_BUFFER_DURATION (buffer);

  in_seg = gst_segment_clip (dec->segment, GST_FORMAT_TIME,
      start, stop, &clip_start, &clip_stop);

  if (in_seg) {
    guint start_offset = 0, stop_offset = buf_size;

    /* We have to remove some heading samples */
    if (clip_start > start && clip_start <= stop) {
      start_offset = gst_util_uint64_scale_int (clip_start - start,
          dec->rate, GST_SECOND) * 2;
      start_offset %= dec->channels;
    }
    /* We have to remove some trailing samples */
    if (clip_stop < stop && clip_stop >= start) {
      stop_offset -= gst_util_uint64_scale_int (stop - clip_stop,
          dec->rate, GST_SECOND) * 2;
      stop_offset %= dec->channels;
    }
    /* Truncating */
    if ((start_offset != 0) || (stop_offset != buf_size)) {
      GstBuffer *subbuf = gst_buffer_create_sub (buffer, start_offset,
          stop_offset - start_offset);
      if (subbuf) {
        gst_buffer_unref (buffer);
        buffer = subbuf;
        /* gst_buffer_create_sub don't crate it with caps */
        gst_buffer_set_caps (buffer, GST_PAD_CAPS (dec->srcpad));
      }
    }

    GST_BUFFER_TIMESTAMP (buffer) = clip_start;
    GST_BUFFER_DURATION (buffer) = clip_stop - clip_start;

    if (dec->discont) {
      GST_DEBUG_OBJECT (dec, "mark this frame with the discont flag");
      GST_BUFFER_FLAG_SET (buffer, GST_BUFFER_FLAG_DISCONT);
      dec->discont = FALSE;
    }

    GST_DEBUG_OBJECT (dec, "push a buffer with timestamp %" GST_TIME_FORMAT
        ", duration %" GST_TIME_FORMAT ", samples %d",
        GST_TIME_ARGS (GST_BUFFER_TIMESTAMP (buffer)),
        GST_TIME_ARGS (GST_BUFFER_DURATION (buffer)),
        (stop_offset - start_offset) / 2);

    dec->last_timestamp = clip_stop;
  } else {
    GST_DEBUG_OBJECT (dec, "dropping buffer because is out of segment,"
        "start %" GST_TIME_FORMAT " stop %" GST_TIME_FORMAT,
        GST_TIME_ARGS (start), GST_TIME_ARGS (stop));
    gst_buffer_unref (buffer);
    buffer = NULL;
  }

  return buffer;
}

static GstFlowReturn
gst_fluadpcmdec_decode (GstFluAdpcmDec * dec)
{
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_GET_CLASS (dec);
  GstFlowReturn ret = GST_FLOW_OK;
  const guint8 *in_data;
  GstBuffer *buffer;
  guint avail = 0;
  gboolean parsing_complete;

  if (dec->chunk_size == 0) {
    GST_WARNING_OBJECT (dec, "chunk_size is yet unknown");
    ret = GST_FLOW_ERROR;
    goto beach;
  }

  avail = gst_adapter_available (dec->adapter);
  /* ensure there is enough data */
  if (avail < dec->chunk_size) {
    GST_LOG_OBJECT (dec, "not enough data for a complete chunk "
        "(have %d need %d)", avail, dec->chunk_size);
    ret = GST_FLOW_NEED_MORE_DATA;
    goto beach;
  }
  /* read the chunk */
  in_data = gst_adapter_peek (dec->adapter, dec->chunk_size);
  if (dec_class->parse_chunk == NULL) {
    GST_ERROR_OBJECT (dec, "parse_chunk virtual method not initialized");
    ret = GST_FLOW_ERROR;
    goto beach;
  }

  GST_MEMDUMP ("Received chunk:", in_data, dec->chunk_size);

  parsing_complete = dec_class->parse_chunk (dec, in_data);

  /* flush the packet in the adapter */
  gst_adapter_flush (dec->adapter, dec->chunk_size);

  if (parsing_complete) {
    /* allocate a buffer for the output samples */
    buffer = gst_buffer_new_and_alloc (dec->out_size);
    if (!GST_IS_BUFFER (buffer)) {
      GST_WARNING_OBJECT (dec, "failed allocating a %" G_GSIZE_FORMAT
          " bytes buffer", dec->out_size);
      ret = GST_FLOW_ERROR;
      goto beach;
    }
    gst_buffer_set_caps (buffer, GST_PAD_CAPS (dec->srcpad));
    /* calculate timestamp and duration */
    GST_BUFFER_TIMESTAMP (buffer) = dec->last_timestamp;
    GST_BUFFER_DURATION (buffer) = dec->duration;
    if (dec->last_timestamp != -1) {
      dec->last_timestamp += GST_BUFFER_DURATION (buffer);
    }

    /* Perform decoding from csample_stream to output buffer. Use custom
     * decoding function if provided, otherwise, use default IMA decoder */
    ret = dec_class->decode_chunk (dec, GST_BUFFER_DATA (buffer));

    GST_MEMDUMP ("Deinterleaved chunk[0]:", dec->csample_stream[0],
        dec->num_compressed_samples);
    if (dec->channels > 1) {
      GST_MEMDUMP ("Deinterleaved chunk[1]:", dec->csample_stream[1],
          dec->num_compressed_samples);
    }
    GST_MEMDUMP ("Produced buffer:", GST_BUFFER_DATA (buffer),
        GST_BUFFER_SIZE (buffer));

    if (ret != GST_FLOW_OK) {
      GST_WARNING_OBJECT (dec, "decoding failed");
      gst_buffer_unref (buffer);
      buffer = NULL;
      goto beach;
    }
    /* perform clipping in segment and push */
    if ((buffer = gst_fluadpcmdec_clip (dec, buffer))) {
      ret = gst_pad_push (dec->srcpad, buffer);
    }
  }
beach:
  return ret;
}

static GstFlowReturn
gst_fluadpcmdec_chain (GstPad * pad, GstBuffer * buffer)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (gst_pad_get_parent (pad));
  GstFlowReturn ret = GST_FLOW_OK;

  GST_DEBUG_OBJECT (dec, "new buffer in chain (size %d bytes)",
      GST_BUFFER_SIZE (buffer));

  /* discontinuity clears adapter, FIXME, maybe we can set some
   * encoder flag to mask the discont. */
  if (GST_BUFFER_FLAG_IS_SET (buffer, GST_BUFFER_FLAG_DISCONT)) {
    gst_adapter_clear (dec->adapter);
    dec->last_timestamp = 0;
    dec->discont = TRUE;
  }

  if (GST_BUFFER_TIMESTAMP_IS_VALID (buffer)) {
    dec->last_timestamp = GST_BUFFER_TIMESTAMP (buffer);
  }

  gst_adapter_push (dec->adapter, buffer);

  while (ret == GST_FLOW_OK) {
    ret = gst_fluadpcmdec_decode (dec);
  }
  if (ret == GST_FLOW_NEED_MORE_DATA) {
    GST_LOG_OBJECT (dec, "need more data");
    ret = GST_FLOW_OK;
  }

  gst_object_unref (dec);

  return ret;
}

static void
gst_fluadpcmdec_flush (GstFluAdpcmDec * dec, gboolean discard)
{
  if (!discard) {
    guint avail = gst_adapter_available (dec->adapter);
    if (avail > 0 && avail < dec->chunk_size) {
      /* Add padding so last chunk can be successfully flushed */
      guint size = dec->chunk_size - avail;
      GstBuffer *buffer = gst_buffer_new_and_alloc (size);
      memset (GST_BUFFER_DATA (buffer), 0, size);
      gst_adapter_push (dec->adapter, buffer);
      gst_fluadpcmdec_decode (dec);
    }
  }
  dec->last_timestamp = 0;
  dec->discont = FALSE;
  dec->eos = FALSE;
  gst_adapter_clear (dec->adapter);
}

static gboolean
gst_fluadpcmdec_event (GstPad * pad, GstEvent * event)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (gst_pad_get_parent (pad));
  gboolean ret = FALSE;

  switch (GST_EVENT_TYPE (event)) {
    case GST_EVENT_EOS:
    {
      GST_DEBUG_OBJECT (dec, "we are EOS");
      gst_fluadpcmdec_flush (dec, FALSE);
      dec->eos = TRUE;
      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_NEWSEGMENT:
    {
      GstFormat format;
      gdouble rate;
      gint64 start, stop, time;
      gboolean update;

      gst_event_parse_new_segment (event, &update, &rate, &format, &start,
          &stop, &time);

      GST_DEBUG_OBJECT (dec, "received new segment from %" GST_TIME_FORMAT
          " to %" GST_TIME_FORMAT, GST_TIME_ARGS (start), GST_TIME_ARGS (stop));

      if (!update) {
        gst_fluadpcmdec_flush (dec, FALSE);
        if (format == GST_FORMAT_TIME) {
          dec->last_timestamp = start;
        }
      }
      if (format != GST_FORMAT_TIME) {
        format = GST_FORMAT_TIME;
        start = time = 0;
        stop = -1;
        gst_event_unref (event);
        event = gst_event_new_new_segment (update, rate, format,
            start, stop, time);
      }
      /* now copy over the values */
      gst_segment_set_newsegment (dec->segment, update, rate, format, start,
          stop, time);


      ret = gst_pad_event_default (pad, event);
      break;
    }
    case GST_EVENT_FLUSH_STOP:
      GST_DEBUG_OBJECT (dec, "flushing");

      gst_fluadpcmdec_flush (dec, TRUE);

      /* Need to init our segment again after a flush */
      gst_segment_init (dec->segment, GST_FORMAT_TIME);

      ret = gst_pad_event_default (pad, event);
      break;
    default:
      ret = gst_pad_event_default (pad, event);
      break;
  }

  gst_object_unref (dec);

  return ret;
}

static GstStateChangeReturn
gst_fluadpcmdec_change_state (GstElement * element, GstStateChange transition)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (element);
  GstStateChangeReturn ret;

  switch (transition) {
    case GST_STATE_CHANGE_NULL_TO_READY:
      break;
    case GST_STATE_CHANGE_READY_TO_PAUSED:
      dec->last_timestamp = 0;
      gst_fluadpcmdec_flush (dec, TRUE);
      break;
    case GST_STATE_CHANGE_PAUSED_TO_PLAYING:
      break;
    default:
      break;
  }

  ret = GST_ELEMENT_CLASS (parent_class)->change_state (element, transition);
  if (ret == GST_STATE_CHANGE_FAILURE)
    return ret;

  switch (transition) {
    case GST_STATE_CHANGE_PAUSED_TO_READY:
      _stop (dec);
      break;
    case GST_STATE_CHANGE_READY_TO_NULL:
      break;
    default:
      break;
  }

  return ret;
}

static void
gst_fluadpcmdec_dispose (GObject * object)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (object);

  if (dec->adapter) {
    gst_adapter_clear (dec->adapter);
    g_object_unref (dec->adapter);
    dec->adapter = NULL;
  }

  if (dec->segment) {
    gst_segment_free (dec->segment);
    dec->segment = NULL;
  }

  G_OBJECT_CLASS (parent_class)->dispose (object);
}

static void
gst_fluadpcmdec_class_init (GstFluAdpcmDecClass * klass)
{
  GstElementClass *gstelement_class = GST_ELEMENT_CLASS (klass);
  GObjectClass *object_class = G_OBJECT_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  klass->parse_chunk = NULL;
  klass->set_format = NULL;
  klass->decode_chunk = _default_ima_decode_chunk;

  object_class->dispose = gst_fluadpcmdec_dispose;

  gstelement_class->change_state =
      GST_DEBUG_FUNCPTR (gst_fluadpcmdec_change_state);
}

static void
gst_fluadpcmdec_init (GstFluAdpcmDec * dec, GstFluAdpcmDecClass * dec_class)
{
  /* Sink pad */
  dec->sinkpad =
      gst_pad_new_from_template (gst_element_class_get_pad_template
      (GST_ELEMENT_CLASS (dec_class), "sink"), "sink");

  gst_pad_set_event_function (dec->sinkpad, gst_fluadpcmdec_event);
  gst_pad_set_setcaps_function (dec->sinkpad, gst_fluadpcmdec_setcaps);
  gst_pad_set_chain_function (dec->sinkpad, gst_fluadpcmdec_chain);

  gst_element_add_pad (GST_ELEMENT (dec), dec->sinkpad);

  /* Src pad */
  dec->srcpad =
      gst_pad_new_from_template (gst_element_class_get_pad_template
      (GST_ELEMENT_CLASS (dec_class), "src"), "src");

  gst_pad_use_fixed_caps (dec->srcpad);
  gst_element_add_pad (GST_ELEMENT (dec), dec->srcpad);

  /* Element initial state */
  dec->adapter = gst_adapter_new ();
  dec->segment = gst_segment_new ();
  dec->last_timestamp = 0;
  dec->eos = FALSE;
  _cleanup (dec);
}
