/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifndef __GST_FLUADPCMIMADVIDEC_H__
#define __GST_FLUADPCMIMADVIDEC_H__

#include "fluadpcmdec.h"

G_BEGIN_DECLS

#define GST_FLUADPCMIMADVIDEC_TYPE            (gst_fluadpcmimadvidec_get_type ())
#define GST_FLUADPCMIMADVIDEC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUADPCMIMADVIDEC_TYPE, GstFluAdpcmImaDviDec))
#define GST_FLUADPCMIMADVIDEC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUADPCMIMADVIDEC_TYPE, GstFluAdpcmImaDviDecClass))
#define GST_IS_FLUADPCMIMADVIDEC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUADPCMIMADVIDEC_TYPE))
#define GST_IS_FLUADPCMIMADVIDEC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUADPCMIMADVIDEC_TYPE))
#define GST_FLUADPCMIMADVIDEC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUADPCMIMADVIDEC_TYPE, GstFluAdpcmImaDviDecClass))
typedef struct _GstFluAdpcmImaDviDec GstFluAdpcmImaDviDec;
typedef struct _GstFluAdpcmImaDviDecClass GstFluAdpcmImaDviDecClass;

#define IMA_DVI_ADPCM_PREAMBLE_SIZE 4   /* In bytes */

struct _GstFluAdpcmImaDviDecClass
{
  GstFluAdpcmDecClass parent_class;
};

struct _GstFluAdpcmImaDviDec
{
  GstFluAdpcmDec parent;
};

GType gst_fluadpcmimadvidec_get_type (void);

G_END_DECLS

#endif /* __GST_FLUADPCMIMADVIDEC_H__ */
