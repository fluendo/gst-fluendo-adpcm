/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fluadpcmmsdec.h"
#include <memory.h>

GST_DEBUG_CATEGORY_EXTERN (gst_fluadpcmmsdec_debug);
#define GST_CAT_DEFAULT gst_fluadpcmmsdec_debug

static void gst_fluadpcmmsdec_init (GstFluAdpcmMsDec * dec);
#define parent_class gst_fluadpcmmsdec_parent_class
G_DEFINE_TYPE (GstFluAdpcmMsDec, gst_fluadpcmmsdec, GST_FLUADPCMDEC_TYPE);

static int FluAdpcmMsAdaptTable[] = {
  230, 230, 230, 230, 307, 409, 512, 614,
  768, 614, 512, 409, 307, 230, 230, 230
};

static int FluAdpcmMsDefaultCoeffTable[] = {
  256, 0, 512, -256, 0, 0, 192, 64, 240, 0, 460, -208, 392, -232
};

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-adpcm, "
        "rate = (int) [ 4000, 96000 ], "
        "channels = (int) { 1,2 }, " "layout = (string)microsoft")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (AUDIO_SRC_CAPS)
    );

static inline void
_load_default_coefs (GstFluAdpcmMsDec * msdec)
{
  msdec->coeff_table_len = sizeof (FluAdpcmMsDefaultCoeffTable) / 2;

  if (msdec->coeff_table)
    g_free (msdec->coeff_table);
  msdec->coeff_table = g_malloc (msdec->coeff_table_len * 2);

  memcpy (msdec->coeff_table, FluAdpcmMsDefaultCoeffTable,
      msdec->coeff_table_len * 2);
}

static inline void
_load_code_data_coefs (GstFluAdpcmMsDec * msdec, guint8 * data, gsize size)
{
  gint i, entries;

  if (size < 4) {
    GST_WARNING_OBJECT (msdec,
        "MS-ADPCM needs 32 bytes of additional codec data");
    return;
  }

  msdec->coeff_table_len = (size - 4) >> 1;

  if (msdec->coeff_table) {
    g_free (msdec->coeff_table);
  }

  msdec->coeff_table = g_malloc (msdec->coeff_table_len * 2);
  data += 4;
  for (i = 0; i < msdec->coeff_table_len; i++) {
    msdec->coeff_table[i] = GST_READ_UINT16_LE (data);
    data += 2;
  }

  entries = msdec->coeff_table_len / 2;
  GST_DEBUG_OBJECT (msdec, "coefficient table has %d entries:", entries);

  for (i = 0; i < entries; i++) {
    GST_DEBUG_OBJECT (msdec, "   %d: %4d,%4d", i,
        msdec->coeff_table[i * 2 + 0], msdec->coeff_table[i * 2 + 1]);
  }
}

static void
gst_fluadpcmmsdec_set_format (GstFluAdpcmDec * dec, GstCaps * caps)
{
  GstStructure *structure = gst_caps_get_structure (caps, 0);
  gint chunk_size;
  const GValue *value;
  GstFluAdpcmMsDec *msdec = GST_FLUADPCMMSDEC (dec);

  if (gst_structure_get_int (structure, "block_align", &chunk_size))
    dec->chunk_size = chunk_size;

  value = gst_structure_get_value (structure, "codec_data");
  if (value) {
    GstBuffer *buf = gst_value_get_buffer (value);
    if (GST_IS_BUFFER (buf)) {
#if GST_CHECK_VERSION(1,0,0)
      GstMapInfo info;
      gst_buffer_map (buf, &info, GST_MAP_READ);
      _load_code_data_coefs (msdec, info.data, gst_buffer_get_size (buf));
      gst_buffer_unmap (buf, &info);
#else
      _load_code_data_coefs (msdec, GST_BUFFER_DATA (buf),
          GST_BUFFER_SIZE (buf));
#endif
    }
  } else {
    GST_DEBUG_OBJECT (dec,
        "codec_data not present in caps. Using default table.");
    _load_default_coefs (msdec);
  }

  dec->num_compressed_samples =
      ((chunk_size -
          MICROSOFT_ADPCM_PREAMBLE_SIZE * dec->channels) / dec->channels) * 2 +
      2;
}

static gboolean
gst_fluadpcmmsdec_parse_chunk (GstFluAdpcmDec * dec, const guint8 * data)
{
  GstFluAdpcmMsDec *msdec = GST_FLUADPCMMSDEC (dec);
  guint32 chunk_size = dec->chunk_size;
  gint channel, nchanels = dec->channels;
  gint num_bytes = chunk_size - MICROSOFT_ADPCM_PREAMBLE_SIZE * nchanels;

  GST_DEBUG_OBJECT (dec, "Parsing %d bytes per channel (%d channels)",
      num_bytes, dec->channels);

  for (channel = 0; channel < nchanels; channel++) {
    msdec->predictor_ndx[channel] = *data++;
    if (msdec->predictor_ndx[channel] >= msdec->coeff_table_len / 2)
      msdec->predictor_ndx[channel] = msdec->coeff_table_len / 2;
  }

  for (channel = 0; channel < nchanels; channel++) {
    msdec->initial_delta[channel] = GST_READ_UINT16_LE (data);
    data += 2;
  }

  for (channel = 0; channel < nchanels; channel++) {
    msdec->initial_samp1[channel] = GST_READ_UINT16_LE (data);
    data += 2;
  }

  for (channel = 0; channel < nchanels; channel++) {
    msdec->initial_samp2[channel] = GST_READ_UINT16_LE (data);
    data += 2;
  }

  if (nchanels == 1) {
    adpcm_denibble_top_first (data, dec->csample_stream[0], num_bytes * 2);
  } else {
    int i;
    for (i = 0; i < num_bytes; i++) {
      dec->csample_stream[0][i] = data[i] >> 4;
      dec->csample_stream[1][i] = data[i] & 0x0F;
    }
  }

  return TRUE;
}

static GstFlowReturn
gst_fluadpcmmsdec_decode_chunk (GstFluAdpcmDec * dec, guint8 * out_data)
{
  GstFluAdpcmMsDec *msdec = GST_FLUADPCMMSDEC (dec);
  gint predicted_sample;
  gint processed_samples;
  gint16 *out_ptr;
  guint8 *in_ptr;
  gint channel, nchanels = dec->channels;
  gint16 samp1;
  gint16 samp2;
  gint32 coef1;
  gint32 coef2;
  gint32 error_delta;
  gint32 signed_error_delta;
  gint32 delta;
  gint32 new_sample;

  GST_DEBUG_OBJECT (dec, "Decoding %d samples", dec->num_compressed_samples);
  for (channel = 0; channel < nchanels; channel++) {
    out_ptr = (gint16 *) out_data + channel;
    *out_ptr = samp2 = msdec->initial_samp2[channel];
    out_ptr += nchanels;
    *out_ptr = samp1 = msdec->initial_samp1[channel];
    out_ptr += nchanels;
    processed_samples = 2;
    in_ptr = dec->csample_stream[channel];
    delta = msdec->initial_delta[channel];

    if (msdec->predictor_ndx[channel] >= msdec->coeff_table_len / 2) {
      GST_WARNING_OBJECT (dec,
          "Predictor index (%d) out of bounds found in header [0,%d]",
          msdec->predictor_ndx[channel], msdec->coeff_table_len / 2 - 1);
      return GST_FLOW_OK;
    }
    coef1 = msdec->coeff_table[msdec->predictor_ndx[channel] * 2 + 0];
    coef2 = msdec->coeff_table[msdec->predictor_ndx[channel] * 2 + 1];

    while (processed_samples < dec->num_compressed_samples) {
      /* Shift because coeffs are stored in 8.8 fixed point */
      predicted_sample = (samp1 * coef1 + samp2 * coef2) >> 8;
      error_delta = *in_ptr;
      /* turn unsigned nibble into signed */
      signed_error_delta =
          (error_delta & 0x08) ? error_delta - 0x10 : error_delta;
      in_ptr++;
      new_sample = predicted_sample + delta * signed_error_delta;
      if (new_sample > G_MAXINT16)
        new_sample = G_MAXINT16;
      if (new_sample < G_MININT16)
        new_sample = G_MININT16;
      *out_ptr = new_sample;
      out_ptr += dec->channels;

      /* Again shift because table is stored in 8.8 fixed point */
      delta = (delta * FluAdpcmMsAdaptTable[error_delta]) >> 8;
      if (delta < 16)
        delta = 16;
      samp2 = samp1;
      samp1 = new_sample;

      processed_samples++;
    }
  }
  return GST_FLOW_OK;
}

static void
gst_fluadpcmmsdec_finalize (GObject * object)
{
  GstFluAdpcmMsDec *msdec = GST_FLUADPCMMSDEC (object);

  if (msdec->coeff_table)
    g_free (msdec->coeff_table);
  msdec->coeff_table = NULL;

  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gst_fluadpcmmsdec_class_init (GstFluAdpcmMsDecClass * klass)
{
  GObjectClass *object_class = G_OBJECT_CLASS (klass);
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  /* Set VM pointers here */
  dec_class->set_format = gst_fluadpcmmsdec_set_format;
  dec_class->parse_chunk = gst_fluadpcmmsdec_parse_chunk;
  dec_class->decode_chunk = gst_fluadpcmmsdec_decode_chunk;

  object_class->finalize = gst_fluadpcmmsdec_finalize;

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));

  gst_element_class_set_details_simple (element_class,
      "Fluendo Microsoft ADPCM Decoder",
      "Codec/Decoder/Audio",
      "Decode Microsoft ADPCM streams to raw audio samples",
      "Fluendo S.A. <support@fluendo.com>");
}

static void
gst_fluadpcmmsdec_init (GstFluAdpcmMsDec * dec)
{
  dec->coeff_table = NULL;
}
