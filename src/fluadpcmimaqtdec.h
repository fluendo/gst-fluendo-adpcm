/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#ifndef __GST_FLUADPCMIMAQTDEC_H__
#define __GST_FLUADPCMIMAQTDEC_H__

#include "fluadpcmdec.h"

G_BEGIN_DECLS

#define GST_FLUADPCMIMAQTDEC_TYPE            (gst_fluadpcmimaqtdec_get_type ())
#define GST_FLUADPCMIMAQTDEC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUADPCMIMAQTDEC_TYPE, GstFluAdpcmImaQtDec))
#define GST_FLUADPCMIMAQTDEC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUADPCMIMAQTDEC_TYPE, GstFluAdpcmImaQtDecClass))
#define GST_IS_FLUADPCMIMAQTDEC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUADPCMIMAQTDEC_TYPE))
#define GST_IS_FLUADPCMIMAQTDEC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUADPCMIMAQTDEC_TYPE))
#define GST_FLUADPCMIMAQTDEC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUADPCMIMAQTDEC_TYPE, GstFluAdpcmImaQtDecClass))
typedef struct _GstFluAdpcmImaQtDec GstFluAdpcmImaQtDec;
typedef struct _GstFluAdpcmImaQtDecClass GstFluAdpcmImaQtDecClass;

#define DEFAULT_IMA_QT_ADPCM_CHUNK_SIZE 34      /* In bytes */
#define IMA_QT_ADPCM_PREAMBLE_SIZE 2    /* In bytes */

struct _GstFluAdpcmImaQtDecClass
{
  GstFluAdpcmDecClass parent_class;
};

struct _GstFluAdpcmImaQtDec
{
  GstFluAdpcmDec parent;
};

GType gst_fluadpcmimaqtdec_get_type (void);

G_END_DECLS

#endif /* __GST_FLUADPCMIMAQTDEC_H__ */
