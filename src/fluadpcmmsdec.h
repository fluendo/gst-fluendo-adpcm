/*
 * FLUENDO S.A.
 * Copyright (C) <2010>  <support@fluendo.com>
 */

#ifndef __GST_FLUADPCMMSDEC_H__
#define __GST_FLUADPCMMSDEC_H__

#include "fluadpcmdec.h"

G_BEGIN_DECLS

#define GST_FLUADPCMMSDEC_TYPE            (gst_fluadpcmmsdec_get_type ())
#define GST_FLUADPCMMSDEC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUADPCMMSDEC_TYPE, GstFluAdpcmMsDec))
#define GST_FLUADPCMMSDEC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUADPCMMSDEC_TYPE, GstFluAdpcmMsDecClass))
#define GST_IS_FLUADPCMMSDEC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUADPCMMSDEC_TYPE))
#define GST_IS_FLUADPCMMSDEC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUADPCMMSDEC_TYPE))
#define GST_FLUADPCMMSDEC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUADPCMMSDEC_TYPE, GstFluAdpcmMsDecClass))
typedef struct _GstFluAdpcmMsDec GstFluAdpcmMsDec;
typedef struct _GstFluAdpcmMsDecClass GstFluAdpcmMsDecClass;

#define MICROSOFT_ADPCM_PREAMBLE_SIZE 7 /* In bytes */

struct _GstFluAdpcmMsDecClass
{
  GstFluAdpcmDecClass parent_class;
};

struct _GstFluAdpcmMsDec
{
  GstFluAdpcmDec parent;

  gint16 *coeff_table;
  gint coeff_table_len;         /* In 16-bits entries */

  guint8 predictor_ndx[2];
  gint16 initial_delta[2];
  gint16 initial_samp1[2];
  gint16 initial_samp2[2];
};

GType gst_fluadpcmmsdec_get_type (void);

G_END_DECLS

#endif /* __GST_FLUADPCMMSDEC_H__ */
