/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fluadpcmimadvidec.h"

GST_DEBUG_CATEGORY_EXTERN (gst_fluadpcmimadvidec_debug);
#define GST_CAT_DEFAULT gst_fluadpcmimadvidec_debug

static void gst_fluadpcmimadvidec_init (GstFluAdpcmImaDviDec * dec);
#define parent_class gst_fluadpcmimadvidec_parent_class
G_DEFINE_TYPE (GstFluAdpcmImaDviDec, gst_fluadpcmimadvidec,
    GST_FLUADPCMDEC_TYPE);

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-adpcm, "
        "rate = (int) [ 4000, 96000 ], "
        "channels = (int) { 1,2 }, " "layout = (string)dvi")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (AUDIO_SRC_CAPS)
    );

static void
gst_fluadpcmimadvidec_set_format (GstFluAdpcmDec * dec, GstCaps * caps)
{
  GstStructure *structure = gst_caps_get_structure (caps, 0);
  gint chunk_size;

  if (gst_structure_get_int (structure, "block_align", &chunk_size))
    dec->chunk_size = chunk_size;

  dec->num_compressed_samples = (
      (chunk_size - IMA_DVI_ADPCM_PREAMBLE_SIZE * dec->channels) /
      dec->channels) * 2;
}

static gboolean
gst_fluadpcmimadvidec_parse_chunk (GstFluAdpcmDec * dec, const guint8 * data)
{
  guint32 chunk_size = dec->chunk_size;
  gint num_bytes = chunk_size - IMA_DVI_ADPCM_PREAMBLE_SIZE * dec->channels;
  gint32 initial_step_index;
  gint channel;

  /* We are interested in the number of bytes PER CHANNEL */
  num_bytes /= dec->channels;

  GST_DEBUG_OBJECT (dec, "Parsing %d bytes per channel (%d channels)",
      num_bytes, dec->channels);

  for (channel = 0; channel < dec->channels; channel++) {
    GST_DEBUG_OBJECT (dec, "Channel %d:", channel);
    dec->initial_predictor[channel] = (gint16) ((data[1] << 8) | data[0]);
    initial_step_index = data[2];
    if (initial_step_index > 88)
      initial_step_index = 88;
    dec->initial_step_index[channel] = initial_step_index;
    GST_DEBUG_OBJECT (dec, "  -> initial_predictor = 0x%x",
        dec->initial_predictor[channel]);
    GST_DEBUG_OBJECT (dec, "  -> initial_step_index = 0x%x",
        dec->initial_step_index[channel]);
    data += IMA_DVI_ADPCM_PREAMBLE_SIZE;
  }
  if (dec->channels == 1) {
    /* Special fast way of doing this */
    adpcm_denibble_bottom_first (data, dec->csample_stream[0], num_bytes * 2);
  } else {
    /* Denibble 8 nibbles (4 bytes) at a time, because channels are
     * interleaved like that */
    gint32 current_byte = 0;
    while (current_byte < num_bytes) {
      for (channel = 0; channel < dec->channels; channel++) {
        adpcm_denibble_bottom_first (data,
            dec->csample_stream[channel] + current_byte * 2, 8);
        data += 4;
      }
      current_byte += 4;
    }
  }

  return TRUE;
}

static void
gst_fluadpcmimadvidec_class_init (GstFluAdpcmImaDviDecClass * klass)
{
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  /* Set VM pointers here */
  dec_class->set_format = gst_fluadpcmimadvidec_set_format;
  dec_class->parse_chunk = gst_fluadpcmimadvidec_parse_chunk;

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));

  gst_element_class_set_details_simple (element_class,
      "Fluendo ADPCM IMA DVI Decoder",
      "Codec/Decoder/Audio",
      "Decode ADPCM IMA DVI streams to raw audio samples",
      "Fluendo S.A. <support@fluendo.com>");
}

static void
gst_fluadpcmimadvidec_init (GstFluAdpcmImaDviDec * dec)
{
}
