/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifndef __GST_FLUADPCMCOMMON_H__
#define __GST_FLUADPCMCOMMON_H__

#include <gst/gst.h>

#include "gst-compat.h"

G_BEGIN_DECLS

#define GST_FLOW_NEED_MORE_DATA -101

/* Expands an array of nibbles, bottom nibble first */
    static inline void
adpcm_denibble_bottom_first (const guint8 * in_nibbles, guint8 * out_bytes,
    gint num_nibbles)
{

  gint i;

  for (i = 0; i < num_nibbles; i += 2) {
    *out_bytes++ = (*in_nibbles) & 0x0F;
    *out_bytes++ = (*in_nibbles) >> 4;
    in_nibbles++;
  }
}

/* Expands an array of nibbles, top nibble first */
static inline void
adpcm_denibble_top_first (const guint8 * in_nibbles, guint8 * out_bytes,
    gint num_nibbles)
{

  gint i;

  for (i = 0; i < num_nibbles; i += 2) {
    *out_bytes++ = (*in_nibbles) >> 4;
    *out_bytes++ = (*in_nibbles) & 0x0F;
    in_nibbles++;
  }
}

G_END_DECLS

#endif /* __GST_FLUADPCMCOMMON_H__ */
