/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "fluadpcmimaqtdec.h"

GST_DEBUG_CATEGORY_EXTERN (gst_fluadpcmimaqtdec_debug);
#define GST_CAT_DEFAULT gst_fluadpcmimaqtdec_debug

static void gst_fluadpcmimaqtdec_init (GstFluAdpcmImaQtDec * dec);
#define parent_class gst_fluadpcmimaqtdec_parent_class
G_DEFINE_TYPE (GstFluAdpcmImaQtDec, gst_fluadpcmimaqtdec, GST_FLUADPCMDEC_TYPE);

static GstStaticPadTemplate sink_factory = GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS ("audio/x-adpcm, "
        "rate = (int) [ 4000, 96000 ], "
        "channels = (int) { 1,2 }, " "layout = (string)quicktime")
    );

static GstStaticPadTemplate src_factory = GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (AUDIO_SRC_CAPS)
    );

static void
gst_fluadpcmimaqtdec_set_format (GstFluAdpcmDec * dec, GstCaps * caps)
{
  /* Deafult chunk size for QT IMA files, if nothing else is specified */
  dec->chunk_size = DEFAULT_IMA_QT_ADPCM_CHUNK_SIZE * dec->channels;

  dec->num_compressed_samples =
      (DEFAULT_IMA_QT_ADPCM_CHUNK_SIZE - IMA_QT_ADPCM_PREAMBLE_SIZE) * 2;
}

static gboolean
gst_fluadpcmimaqtdec_parse_chunk (GstFluAdpcmDec * dec, const guint8 * data)
{
  gint num_nibbles =
      (DEFAULT_IMA_QT_ADPCM_CHUNK_SIZE - IMA_QT_ADPCM_PREAMBLE_SIZE) * 2;
  gint channel, nchannels = dec->channels;

  for (channel = 0; channel < nchannels; channel++) {
    GST_DEBUG_OBJECT (dec, "Parsing %d bytes for channel %d",
        num_nibbles, channel);

    dec->initial_predictor[channel] =
        (gint16) ((data[0] << 8) | (data[1] & 0x80));

    dec->initial_step_index[channel] = MIN (data[1] & 0x7F, 88);

    adpcm_denibble_bottom_first (data + IMA_QT_ADPCM_PREAMBLE_SIZE,
        dec->csample_stream[channel], num_nibbles);
    data += DEFAULT_IMA_QT_ADPCM_CHUNK_SIZE;
  }

  return TRUE;
}

static void
gst_fluadpcmimaqtdec_class_init (GstFluAdpcmImaQtDecClass * klass)
{
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_CLASS (klass);
  GstElementClass *element_class = GST_ELEMENT_CLASS (klass);

  /* Set VM pointers here */
  dec_class->set_format = gst_fluadpcmimaqtdec_set_format;
  dec_class->parse_chunk = gst_fluadpcmimaqtdec_parse_chunk;

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&sink_factory));

  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&src_factory));

  gst_element_class_set_details_simple (element_class,
      "Fluendo ADPCM IMA QT Decoder",
      "Codec/Decoder/Audio",
      "Decode ADPCM IMA QT streams to raw audio samples",
      "Fluendo S.A. <support@fluendo.com>");
}

static void
gst_fluadpcmimaqtdec_init (GstFluAdpcmImaQtDec * dec)
{
}
