/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <string.h>
#include "fluadpcmcommon.h"
#include "fluadpcmdec.h"

GST_DEBUG_CATEGORY_EXTERN (gst_fluadpcmdec_debug);
#define GST_CAT_DEFAULT gst_fluadpcmdec_debug

static gint32 ima_index_table[16] = {
  -1, -1, -1, -1, 2, 4, 6, 8,
  -1, -1, -1, -1, 2, 4, 6, 8
};

static gint32 ima_step_table[89] = {
  7, 8, 9, 10, 11, 12, 13, 14, 16, 17,
  19, 21, 23, 25, 28, 31, 34, 37, 41, 45,
  50, 55, 60, 66, 73, 80, 88, 97, 107, 118,
  130, 143, 157, 173, 190, 209, 230, 253, 279, 307,
  337, 371, 408, 449, 494, 544, 598, 658, 724, 796,
  876, 963, 1060, 1166, 1282, 1411, 1552, 1707, 1878, 2066,
  2272, 2499, 2749, 3024, 3327, 3660, 4026, 4428, 4871, 5358,
  5894, 6484, 7132, 7845, 8630, 9493, 10442, 11487, 12635, 13899,
  15289, 16818, 18500, 20350, 22385, 24623, 27086, 29794, 32767
};

static GstFlowReturn
_default_ima_decode_chunk (GstFluAdpcmDec * dec, guint8 * out_data)
{
  gint32 predictor;
  gint32 step_index;
  gint32 step;
  gint channel, n;
  gint16 *out_ptr;

  for (channel = 0; channel < dec->channels; channel++) {
    out_ptr = (gint16 *) out_data;
    out_ptr += channel;
    predictor = dec->initial_predictor[channel];
    step_index = dec->initial_step_index[channel];
    step = ima_step_table[step_index];
    GST_DEBUG_OBJECT (dec, "initial predictor = 0x%04x", predictor);
    GST_DEBUG_OBJECT (dec, "initial step_index = 0x%04x", step_index);

    for (n = 0; n < dec->num_compressed_samples; n++) {
      gint32 csamp = dec->csample_stream[channel][n];
      gint32 csamp_sign = (csamp & 8) ? -1 : +1;
      gint32 csamp_magn = csamp & 7;
      gint32 diff;

      step_index += ima_index_table[csamp];
      if (step_index < 0)
        step_index = 0;
      if (step_index > 88)
        step_index = 88;
      diff = csamp_sign * (csamp_magn * step + step / 2) / 4;
      predictor += diff;
      if (predictor < -32768)
        predictor = -32768;
      if (predictor > +32767)
        predictor = +32767;
      step = ima_step_table[step_index];

      *out_ptr = predictor;
      out_ptr += dec->channels;
    }

    /* Leave the initial vars prepared for the next chunk, in case they do not
     * get initialized by a preamble or other ways */
    dec->initial_predictor[channel] = predictor;
    dec->initial_step_index[channel] = step_index;
  }

  return GST_FLOW_OK;
}

static inline void
_cleanup (GstFluAdpcmDec * dec)
{
  dec->channels = 0;
  dec->rate = 0;
  dec->chunk_size = 0;
  dec->bits_per_compressed_sample = 4;
  dec->initial_predictor[0] = dec->initial_predictor[1] = 0;
  dec->initial_step_index[0] = dec->initial_step_index[1] = 0;
  dec->out_size = 0;
}

static inline gboolean
_start (GstFluAdpcmDec * dec)
{
  /* By now, chunk_size should be known, allocate temporary storage.
   * Incomming compressed samples will be stored as bytes.
   * Both channels are allocated at once. */
  if (dec->csample_stream[0])
    g_free (dec->csample_stream[0]);
  dec->csample_stream[0] = g_malloc (dec->num_compressed_samples * 2);
  if (dec->csample_stream[0] == NULL) {
    GST_ERROR_OBJECT (dec, "failed allocating %d bytes",
        dec->num_compressed_samples * 2);
    return FALSE;
  }
  dec->csample_stream[1] = dec->csample_stream[0] + dec->num_compressed_samples;
  dec->out_size = dec->num_compressed_samples * 2 * dec->channels;
  return TRUE;
}

static inline void
_stop (GstFluAdpcmDec * dec)
{
  if (dec->csample_stream[0]) {
    g_free (dec->csample_stream[0]);
    dec->csample_stream[0] = dec->csample_stream[1] = NULL;
  }
}

static inline gboolean
_set_format (GstElement * element, GstCaps * caps)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (element);
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_GET_CLASS (dec);
  GstStructure *structure;
  gboolean ret = FALSE;
  gint channels, rate;

  structure = gst_caps_get_structure (caps, 0);

  /* get channel count & rate */
  if (!gst_structure_get_int (structure, "channels", &channels))
    channels = dec->channels;
  if (!gst_structure_get_int (structure, "rate", &rate))
    rate = dec->rate;

  dec->channels = channels;
  dec->rate = rate;

  /* Give the derived class the opportunity to take a look at the caps */
  if (dec_class->set_format)
    dec_class->set_format (dec, caps);

  if (!_start (dec)) {
    GST_WARNING_OBJECT (dec, "failed allocating %d bytes",
        dec->num_compressed_samples * 2);
    goto beach;
  }

  GST_DEBUG_OBJECT (dec, "Compressed samples per chunk = %d",
      dec->num_compressed_samples);

  ret = TRUE;
beach:
  return ret;
}

#if GST_CHECK_VERSION(1,0,0)
#include "fluadpcmdec-1_0.c"
#else
#include "fluadpcmdec-0_10.c"
#endif
