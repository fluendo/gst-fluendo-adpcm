/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

static GstAudioDecoderClass *parent_class = NULL;
static void gst_fluadpcmdec_class_init (GstFluAdpcmDecClass * klass);
static void gst_fluadpcmdec_init (GstFluAdpcmDec * dec,
    GstFluAdpcmDecClass * dec_class);

GType
gst_fluadpcmdec_get_type (void)
{
  static volatile gsize fluadpcmdec_type = 0;

  if (g_once_init_enter (&fluadpcmdec_type)) {
    GType _type;
    static const GTypeInfo fluadpcmdec_info = {
      sizeof (GstFluAdpcmDecClass),
      NULL,
      NULL,
      (GClassInitFunc) gst_fluadpcmdec_class_init,
      NULL,
      NULL,
      sizeof (GstFluAdpcmDec),
      0,
      (GInstanceInitFunc) gst_fluadpcmdec_init,
    };

    _type = g_type_register_static (GST_TYPE_AUDIO_DECODER,
        "GstFluAdpcmDec", &fluadpcmdec_info, G_TYPE_FLAG_ABSTRACT);
    g_once_init_leave (&fluadpcmdec_type, _type);
  }
  return fluadpcmdec_type;
}

static gboolean
gst_fluadpcmdec_start (GstAudioDecoder * base)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (base);

  GST_DEBUG_OBJECT (dec, "start");
  _cleanup (dec);

#if 0
  /* call upon legacy upstream byte support (e.g. seeking) */
  gst_audio_decoder_set_estimate_rate (dec, TRUE);
  /* never mind a few errors */
  gst_audio_decoder_set_max_errors (dec, 10);
#endif
  return TRUE;
}

static gboolean
gst_fluadpcmdec_stop (GstAudioDecoder * base)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (base);

  GST_DEBUG_OBJECT (dec, "stop");
  _cleanup (dec);
  _stop (dec);

  return TRUE;
}

static gboolean
gst_fluadpcmdec_set_format (GstAudioDecoder * base, GstCaps * caps)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (base);
  gboolean ret = FALSE;

  if (_set_format (GST_ELEMENT_CAST (base), caps)) {
    GstAudioInfo ainfo;
    gst_audio_info_set_format (&ainfo, GST_AUDIO_FORMAT_S16, dec->rate,
        dec->channels, NULL);
    ret = gst_audio_decoder_set_output_format (base, &ainfo);
  }

  return ret;
}

static GstFlowReturn
gst_fluadpcmdec_parse (GstAudioDecoder * base, GstAdapter * adapter,
    gint * offset, gint * length)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (base);
  guint size;
  gboolean sync, eos;

  size = gst_adapter_available (adapter);
  g_return_val_if_fail (size > 0, GST_FLOW_ERROR);

  gst_audio_decoder_get_parse_state (base, &sync, &eos);

  if (!eos && size < dec->chunk_size) {
    GST_LOG_OBJECT (dec, "need more data");
    /* Not very intuitive but this is the way to indicate NEED_MORE_DATA */
    return GST_FLOW_EOS;
  }

  *offset = 0;
  *length = (size > dec->chunk_size ? dec->chunk_size : size);
  return GST_FLOW_OK;
}

static GstFlowReturn
gst_fluadpcmdec_handle_frame (GstAudioDecoder * base, GstBuffer * buffer)
{
  GstFluAdpcmDec *dec = GST_FLUADPCMDEC (base);
  GstFluAdpcmDecClass *dec_class = GST_FLUADPCMDEC_GET_CLASS (dec);
  GstFlowReturn ret = GST_FLOW_OK;
  GstMapInfo map;
  guchar *input_data;

  /* no fancy draining */
  if (G_UNLIKELY (!buffer))
    return GST_FLOW_OK;

  gst_buffer_map (buffer, &map, GST_MAP_READ);
  input_data = map.data;

  if (dec_class->parse_chunk (dec, input_data)) {
    GstMapInfo omap;
    GstBuffer *outbuf;

    gst_buffer_unmap (buffer, &map);
    buffer = NULL;

    outbuf = gst_buffer_new_allocate (NULL, dec->out_size, NULL);
    if (outbuf) {
      gst_buffer_map (outbuf, &omap, GST_MAP_READWRITE);
      /* Perform decoding from csample_stream to output buffer. Use custom
       * decoding function if provided, otherwise, use default IMA decoder */
      ret = dec_class->decode_chunk (dec, omap.data);
      gst_buffer_unmap (outbuf, &omap);

      if (ret != GST_FLOW_OK) {
        GST_ERROR_OBJECT (dec, "decoding failed");
        gst_buffer_unref (outbuf);
        ret = GST_FLOW_ERROR;
      } else {
        ret = gst_audio_decoder_finish_frame (base, outbuf, 1);
      }
    } else {
      GST_WARNING_OBJECT (dec, "failed allocating a %" G_GSIZE_FORMAT
          " bytes buffer", dec->out_size);
      ret = GST_FLOW_ERROR;
    }
  }

  if (buffer) {
    gst_buffer_unmap (buffer, &map);
  }

  return ret;
}

static void
gst_fluadpcmdec_flush (GstAudioDecoder * base, gboolean hard)
{
}

static void
gst_fluadpcmdec_class_init (GstFluAdpcmDecClass * klass)
{
  GstAudioDecoderClass *base_class = GST_AUDIO_DECODER_CLASS (klass);

  parent_class = g_type_class_peek_parent (klass);

  base_class->start = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_start);
  base_class->stop = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_stop);
  base_class->set_format = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_set_format);
  base_class->parse = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_parse);
  base_class->handle_frame = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_handle_frame);
  base_class->flush = GST_DEBUG_FUNCPTR (gst_fluadpcmdec_flush);

  klass->parse_chunk = NULL;
  klass->set_format = NULL;
  klass->decode_chunk = _default_ima_decode_chunk;

}

static void
gst_fluadpcmdec_init (GstFluAdpcmDec * dec, GstFluAdpcmDecClass * dec_class)
{
  _cleanup (dec);
}
