/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>

#include "fluadpcmimaqtdec.h"
#include "fluadpcmimadvidec.h"
#include "fluadpcmmsdec.h"

#include "gst-fluendo.h"

GST_DEBUG_CATEGORY (gst_fluadpcmdec_debug);
GST_DEBUG_CATEGORY (gst_fluadpcmimaqtdec_debug);
GST_DEBUG_CATEGORY (gst_fluadpcmimadvidec_debug);
GST_DEBUG_CATEGORY (gst_fluadpcmmsdec_debug);

static gboolean
plugin_init (GstPlugin * plugin)
{
  GST_DEBUG_CATEGORY_INIT (gst_fluadpcmdec_debug, "fluadpcmdec",
      (GST_DEBUG_FG_GREEN), "Fluendo ADPCM decoder base class");

  GST_DEBUG_CATEGORY_INIT (gst_fluadpcmimaqtdec_debug, "fluadpcmimaqtdec",
      (GST_DEBUG_FG_GREEN), "Fluendo ADPCM IMA QT decoder");

  GST_DEBUG_CATEGORY_INIT (gst_fluadpcmimadvidec_debug, "fluadpcmimadvidec",
      (GST_DEBUG_FG_GREEN), "Fluendo ADPCM IMA DVI decoder");

  GST_DEBUG_CATEGORY_INIT (gst_fluadpcmmsdec_debug, "fluadpcmmsdec",
      (GST_DEBUG_FG_GREEN), "Fluendo Microsoft ADPCM decoder");

  if (!gst_element_register (plugin, "fluadpcmimaqtdec", GST_RANK_PRIMARY + 1,
          gst_fluadpcmimaqtdec_get_type ()))
    return FALSE;

  if (!gst_element_register (plugin, "fluadpcmimadvidec", GST_RANK_PRIMARY + 1,
          gst_fluadpcmimadvidec_get_type ()))
    return FALSE;

  if (!gst_element_register (plugin, "fluadpcmmsdec", GST_RANK_PRIMARY + 1,
          gst_fluadpcmmsdec_get_type ()))
    return FALSE;

  return TRUE;
}

FLUENDO_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    "fluadpcm",
    fluadpcm,
    "Fluendo ADPCM decoders",
    plugin_init,
    VERSION,
    GST_LICENSE_UNKNOWN, "Fluendo ADPCM decoders", "http://www.fluendo.com");
