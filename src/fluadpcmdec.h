/*
 * FLUENDO S.A.
 * Copyright (C) <2010-2012>  <support@fluendo.com>
 */

#ifndef __GST_FLUADPCMDEC_H__
#define __GST_FLUADPCMDEC_H__

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gst-compat.h"
#include "gst-fluendo.h"

#if GST_CHECK_VERSION(1,0,0)
#include <gst/audio/gstaudiodecoder.h>
#endif

#include "fluadpcmcommon.h"

G_BEGIN_DECLS

#define GST_FLUADPCMDEC_TYPE            (gst_fluadpcmdec_get_type ())
#define GST_FLUADPCMDEC(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GST_FLUADPCMDEC_TYPE, GstFluAdpcmDec))
#define GST_FLUADPCMDEC_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GST_FLUADPCMDEC_TYPE, GstFluAdpcmDecClass))
#define GST_IS_FLUADPCMDEC(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GST_FLUADPCMDEC_TYPE))
#define GST_IS_FLUADPCMDEC_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GST_FLUADPCMDEC_TYPE))
#define GST_FLUADPCMDEC_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GST_FLUADPCMDEC_TYPE, GstFluAdpcmDecClass))

#if GST_CHECK_VERSION(1,0,0)
#define AUDIO_SRC_CAPS \
        "audio/x-raw, " \
        "format = (string) " GST_AUDIO_NE (S16) ", " \
        "layout = (string) interleaved, " \
        "rate = (int) [ 4000, 96000 ], " \
        "channels = (int) { 1,2 } "
#else
#define AUDIO_SRC_CAPS \
        "audio/x-raw-int, " \
        "rate = (int) [ 4000, 96000 ], " \
        "channels = (int) { 1,2 }, " \
        "width = (int) 16, " \
        "depth = (int) 16, " \
        "signed = (boolean) true, " \
        "endianness = (int) BYTE_ORDER"
#endif

typedef struct _GstFluAdpcmDec GstFluAdpcmDec;
typedef struct _GstFluAdpcmDecClass GstFluAdpcmDecClass;

struct _GstFluAdpcmDecClass
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioDecoderClass parent_class;
#else
  GstElementClass parent_class;
#endif

  /* Give the derived class the opportunity to take a look at the caps and
   * extract any information it needs, storing it inside this class */
  void (*set_format) (GstFluAdpcmDec * dec, GstCaps * caps);

  /* Read chunk. Store preamble info inside this class and deinterleave
   * the chunk data directly into the bitstream attribute.
   * Returns TRUE when parsing is complete and decoding can beging. */
  gboolean (*parse_chunk) (GstFluAdpcmDec * dec, const guint8 * data);

  /* Some derived classes might want to do their own decoding instead of using
   * the provided IMA decoder */
  GstFlowReturn (*decode_chunk) (GstFluAdpcmDec * dec, guint8 * out_data);
};

struct _GstFluAdpcmDec
{
#if GST_CHECK_VERSION(1,0,0)
  GstAudioDecoder parent;
#else
  GstElement parent;

  /* Pads */
  GstPad *sinkpad;
  GstPad *srcpad;

  /* Segment */
  GstSegment *segment;

  gboolean discont;
  gboolean eos;

  GstClockTime last_timestamp;
  GstClockTime duration;

  GstAdapter *adapter;
#endif

  /* 1 or 2 */
  guint32 channels;
  /* In signed 16-bit samples per second */
  guint32 rate;
  /* In bytes. Usually read from the container. */
  guint32 chunk_size;
  /* Usually 4, but some codecs might use other values */
  guint8 bits_per_compressed_sample;
  /* Initial values for the decoder.
   * Usually read from a preamble by a derived class */
  gint32 initial_predictor[2];
  gint32 initial_step_index[2];
  /* Stream suitable for decoding. Each compressed sample is put on a single
   * byte (so the top bits are 0), and in the correct order. The parse_chunk
   * method of the derived class has to fill this array.
   * One array per channel. Second one might be unused */
  guint8 *csample_stream[2];
  /* Number of COMPRESSED SAMPLES PER CHANNEL in the csample_stream above.
   * Note that the base class cannot derive this number and must be provided
   * by the derived one. */
  guint32 num_compressed_samples;
  /* Size of output GstBuffer in bytes (precalculated) */  
  gsize out_size;
};

GType gst_fluadpcmdec_get_type (void);

G_END_DECLS

#endif /* __GST_FLUADPCMDEC_H__ */
