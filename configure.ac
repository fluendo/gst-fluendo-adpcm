AC_PREREQ(2.52)

dnl please read gstreamer/docs/random/autotools before changing this file

dnl initialize autoconf
dnl fill in your package name and version here
dnl the fourth (nano) number should be 0 for a release, 1 for CVS,
dnl and 2... for a prerelease
dnl releases only do -Wall, cvs and prerelease does -Werror too
AC_INIT(GStreamer Fluendo ADPCM plugin, 
    m4_esyscmd([common/git-version-gen .tarball-version]),,
    gst-fluendo-adpcm)

AG_GST_INIT

dnl enable silent rules by default
AM_SILENT_RULES([yes])

dnl initialize automake
AM_INIT_AUTOMAKE

dnl define PACKAGE_VERSION_* variables
AS_VERSION

dnl check if this is a release version
AS_NANO(GST_CVS="no", GST_CVS="yes")

dnl define the output header for config
AC_CONFIG_HEADER([config.h])

dnl AM_MAINTAINER_MODE only provides the option to configure to enable it
AM_MAINTAINER_MODE

dnl sets host_* variables
AC_CANONICAL_HOST

dnl make aclocal work in maintainer mode
AC_SUBST(ACLOCAL_AMFLAGS, "-I common/m4")

dnl *** check for arguments to configure ***

AG_GST_ARG_PROFILING
AG_GST_ARG_VALGRIND
AG_GST_ARG_GCOV
AG_GST_ARG_STATIC_PLUGIN

dnl *** checks for platform ***

dnl * hardware/architecture *

dnl check CPU type
AG_GST_ARCH

dnl *** checks for programs ***

AC_PROG_CC
AM_PROG_CC_C_O dnl to compile with per-target flag
AC_DISABLE_STATIC
AC_PROG_LIBTOOL
AM_PROG_AS

AC_PATH_PROG(VALGRIND_PATH, valgrind, no)
AM_CONDITIONAL(HAVE_VALGRIND, test ! "x$VALGRIND_PATH" = "xno")

dnl define an ERROR_CFLAGS Makefile variable
AG_GST_SET_ERROR_CFLAGS($GST_CVS)

dnl Check for pkgconfig first
AC_CHECK_PROG(HAVE_PKGCONFIG, pkg-config, yes, no)

dnl Give error and exit if we don't have pkgconfig
if test "x$HAVE_PKGCONFIG" = "xno"; then
  AC_MSG_ERROR(you need to have pkgconfig installed !)
fi

dnl Now we're ready to ask for gstreamer libs and cflags
dnl And we can also ask for the right version of gstreamer

AG_GST_DETECT_VERSION([1.0.0], [0.10.3])
AG_GST_CHECK_GST($GST_MAJORMINOR, [$GST_REQ])
AG_GST_CHECK_GST_BASE($GST_MAJORMINOR, [$GST_REQ])
AG_GST_CHECK_GST_AUDIO($GST_MAJORMINOR, [$GST_REQ])
AG_GST_CHECK_GST_CHECK($GST_MAJORMINOR, [$GST_REQ], no)

dnl define an ERROR_CFLAGS Makefile variable
AG_GST_SET_ERROR_CFLAGS($GST_CVS)

dnl append GST_ERROR cflags to GST_CFLAGS
GST_CFLAGS="$GST_CFLAGS \$(ERROR_CFLAGS)"

dnl define CPU_TUNE_CFLAGS and CPU_TUNE_CCASFLAGS
AG_GST_CPU_TUNE

dnl add option to enable building with static libstdc++
ARG_WITH_STATIC_LIBSTDCPP

dnl append ERROR_CFLAGS cflags to GST_CFLAGS
GST_CFLAGS="$GST_CFLAGS $ERROR_CFLAGS $GLIB_CFLAGS"

dnl make GST_CFLAGS, GST_CCASFLAGS and GST_LIBS available
AC_SUBST(GST_CFLAGS)
AC_SUBST(GST_CCASFLAGS)
AC_SUBST(GST_LIBS)

dnl make GST_MAJORMINOR available in Makefile.am
AC_SUBST(GST_MAJORMINOR)

AG_GST_SET_PLUGINDIR

dnl set proper LDFLAGS for plugins
GST_PLUGIN_LDFLAGS='-module -avoid-version -export-symbols-regex [_]*\(gst_\|Gst\|GST_\).*'
AC_SUBST(GST_PLUGIN_LDFLAGS)

dnl Start date used to autogenrate ChangeLog from git log
AC_SUBST(GIT_LOG_START_COMMIT, "a1db5e07483eb98a6f7e9204a7d92a8a4f935cac")

dnl check for gtk-doc
m4_ifdef([GTK_DOC_CHECK], [
  GTK_DOC_CHECK([1.14],[--flavour no-tmpl])
],[
  AM_CONDITIONAL([ENABLE_GTK_DOC], false)
])

AC_OUTPUT(
Makefile
src/Makefile
common/Makefile
common/m4/Makefile
win32/Makefile
win32/oa/Makefile
win32/oa/config.h
win32/vs10/Makefile
win32/include/Makefile
win32/include/config-orig.h
docs/Makefile
docs/reference/Makefile
gst-fluendo-adpcm.spec
)

echo "
$PACKAGE-$VERSION

        prefix:                           ${prefix}
        compiler:                         ${CC}
        Building for GStreamer-${GST_MAJORMINOR}

        Build profile:                    ${BUILD_PROFILE}

"

